# Aplicatie BE pentru piese auto
Aplicatia web are la baza 6 tabele. Producatorul de masini, masinile, clientul, piese auto, producatorul pieselor auto si comanda propriu zisa. Intre aceste tabele exista retaliile urmatoare: intre producatorul de masini si masini - one to many, intre client si masini - many to many, intre producatorul de piese auto si pisele auto - one to many, intre clint si comanda - one to many si intre comanda si piese - many to many.

## Aplicatie pune la dispozitie urmatoare functionalitati

- Un utilizator poate sa isi creeze un cont
- Un utilizator poate sa isi creeze o comanda
- Un utilizator poate sa adauge piese auto in comanda
- Se pot adauga piese auto si producatorii acestore
- Se pot edita piesele
- Se pot adauga masini si producatorii lor
- Se pot adauga masinile compatibile fiecaror piese
- Se pot face filtrari diferite pentru piese auto cat si pentru masini
- Utilizatorul poate sa isi adauge ce tip de masini are

Fiecare endpoint este detalitat in colectia postman din repo.
