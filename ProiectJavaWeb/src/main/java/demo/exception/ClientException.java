package demo.exception;

public class ClientException extends RuntimeException {
    public enum ErrorCode {
        CLIENT_ALREADY_EXISTS,
        CLIENT_NOT_FOUND,
        CAR_NOT_FOUND,
        ORDER_NOT_FOUND,
        PART_NOT_FOUND
    }

    private ClientException.ErrorCode error;

    private ClientException(ClientException.ErrorCode error) { this.error = error; }

    public ClientException.ErrorCode getError() { return error; }

    public static ClientException clientAlreadyExists() { return new ClientException(ErrorCode.CLIENT_ALREADY_EXISTS); }
    public static ClientException clientNotFound() { return new ClientException(ClientException.ErrorCode.CLIENT_NOT_FOUND); }
    public static ClientException carNotFound() { return new ClientException(ClientException.ErrorCode.CAR_NOT_FOUND); }
    public static ClientException orderNotFound() { return new ClientException(ClientException.ErrorCode.ORDER_NOT_FOUND); }
    public static ClientException carPartNotFound() { return new ClientException(ClientException.ErrorCode.PART_NOT_FOUND); }
}
