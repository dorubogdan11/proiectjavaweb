package demo.exception;

public class CarPartException extends RuntimeException {
    public enum ErrorCode {
        CAR_PART_ALREADY_EXISTS,
        CAR_PART_MANUFACTURER_NOT_FOUND,
        CAR_PART_NOT_FOUND,
        CAR_NOT_FOUND
    }
    private CarPartException.ErrorCode error;

    private CarPartException(CarPartException.ErrorCode error) { this.error = error; }

    public CarPartException.ErrorCode getError() { return error; }

    public static CarPartException partAlreadyExists() { return new CarPartException(CarPartException.ErrorCode.CAR_PART_ALREADY_EXISTS); }
    public static CarPartException partManufacturerNotFound() { return new CarPartException(CarPartException.ErrorCode.CAR_PART_MANUFACTURER_NOT_FOUND); }
    public static CarPartException partNotFound() { return new CarPartException(CarPartException.ErrorCode.CAR_PART_NOT_FOUND); }
    public static CarPartException carNotFound() { return new CarPartException(CarPartException.ErrorCode.CAR_NOT_FOUND); }
}
