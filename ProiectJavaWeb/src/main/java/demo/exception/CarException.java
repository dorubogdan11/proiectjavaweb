package demo.exception;

public class CarException extends RuntimeException {
    public enum ErrorCode {
        CAR_ALREADY_EXISTS,
        CAR_MANUFACTURER_NOT_FOUND,
        CAR_NOT_FOUND
    }

    private CarException.ErrorCode error;

    private CarException(CarException.ErrorCode error) { this.error = error; }

    public CarException.ErrorCode getError() { return error; }

    public static CarException carAlreadyExists() { return new CarException(ErrorCode.CAR_ALREADY_EXISTS); }
    public static CarException carManufacturerNotFound() { return new CarException(ErrorCode.CAR_MANUFACTURER_NOT_FOUND); }
    public static CarException carNotFound() { return new CarException(ErrorCode.CAR_NOT_FOUND); }
}
