package demo.exception;

public class PartManufacturerException extends RuntimeException {
    public enum ErrorCode {
        MANUFACTURER_ALREADY_EXISTS,
        MANUFACTURER_NOT_FOUND,
        MANUFACTURERS_NOT_FOUND
    }

    private PartManufacturerException.ErrorCode error;

    private PartManufacturerException(PartManufacturerException.ErrorCode error) { this.error = error; }

    public PartManufacturerException.ErrorCode getError() { return error; }

    public static PartManufacturerException manufacturerAlreadyExists() { return new PartManufacturerException(PartManufacturerException.ErrorCode.MANUFACTURER_ALREADY_EXISTS); }
    public static PartManufacturerException manufactureNotFoundException() { return new PartManufacturerException(PartManufacturerException.ErrorCode.MANUFACTURER_NOT_FOUND); }
    public static PartManufacturerException manufacturesNotFoundException() { return new PartManufacturerException(PartManufacturerException.ErrorCode.MANUFACTURERS_NOT_FOUND); }
}
