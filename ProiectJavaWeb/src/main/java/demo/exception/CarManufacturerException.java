package demo.exception;

public class CarManufacturerException extends RuntimeException {
    public enum ErrorCode {
        MANUFACTURER_ALREADY_EXISTS,
        MANUFACTURER_NOT_FOUND,
        MANUFACTURERS_NOT_FOUND
    }

    private ErrorCode error;

    private CarManufacturerException(ErrorCode error) { this.error = error; }

    public ErrorCode getError() { return error; }

    public static CarManufacturerException manufacturerAlreadyExists() { return new CarManufacturerException(ErrorCode.MANUFACTURER_ALREADY_EXISTS); }
    public static CarManufacturerException manufactureNotFoundException() { return new CarManufacturerException(ErrorCode.MANUFACTURER_NOT_FOUND); }
    public static CarManufacturerException manufacturesNotFoundException() { return new CarManufacturerException(ErrorCode.MANUFACTURERS_NOT_FOUND); }
}
