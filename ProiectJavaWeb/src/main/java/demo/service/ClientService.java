package demo.service;

import demo.exception.ClientException;
import demo.models.Car;
import demo.models.CarPart;
import demo.models.ClientOrder;
import demo.models.Clients;
import demo.repository.CarPartRepository;
import demo.repository.CarRepository;
import demo.repository.ClientRepository;
import demo.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CarPartRepository carPartRepository;

    public Clients createClient(Clients newClient) {
        List<Clients> clients = (List<Clients>) clientRepository.findAll();
        if (!clients.isEmpty()) {
            for (Clients client : clients) {
                if (client.getFirstName().equals(newClient.getFirstName())
                && client.getLastName().equals(newClient.getLastName())
                && client.getAddress().equals(newClient.getAddress())) {
                    throw ClientException.clientAlreadyExists();
                }
            }
        }
        return  clientRepository.save(newClient);
    }

    public Clients addCar(long clientId, long carId) {
        Optional<Clients> client = clientRepository.findById(clientId);
        if (client.isPresent()) {
            Optional<Car> car = carRepository.findById(carId);
            if (car.isPresent()) {
                client.get().addCar(car.get());
                return clientRepository.save(client.get());
            } else {
                throw ClientException.carNotFound();
            }
        } else {
            throw ClientException.clientNotFound();
        }
    }

    public ClientOrder createNewOrder(long clientId) {
        Optional<Clients> client = clientRepository.findById(clientId);
        if (client.isPresent()) {
            ClientOrder newOreder = new ClientOrder();
            newOreder.setClient(client.get());
            return orderRepository.save(newOreder);
        } else {
            throw ClientException.clientNotFound();
        }
    }

    public ClientOrder addPartToOrder(long orderId, long partId) {
        Optional<ClientOrder> order = orderRepository.findById(orderId);
        if (order.isPresent()) {
            Optional<CarPart> part = carPartRepository.findById(partId);
            if (part.isPresent()) {
                order.get().addPart(part.get());
                return orderRepository.save(order.get());
            } else {
                throw ClientException.carPartNotFound();
            }
        } else {
            throw ClientException.orderNotFound();
        }
    }

    public ClientOrder deletePartFromOrder(long orderId, long partId) {
        Optional<ClientOrder> order = orderRepository.findById(orderId);
        if (order.isPresent()) {
            Optional<CarPart> part = carPartRepository.findById(partId);
            if (part.isPresent()) {
                Set<CarPart> parts = order.get().getCarParts();
                for(CarPart carPart : parts){
                    if (carPart.getId() == partId) {
                        parts.remove(carPart);
                        break;
                    }
                }
                order.get().setCarParts(parts);
                return orderRepository.save(order.get());
            } else {
                throw ClientException.carPartNotFound();
            }
        } else {
            throw ClientException.orderNotFound();
        }
    }

    public void deleteOrder(long orderId) {
        Optional<ClientOrder> order = orderRepository.findById(orderId);
        if (order.isPresent()) {
            order.get().setClient(null);
            order.get().deleteAllParts();
            orderRepository.delete(orderRepository.save(order.get()));
        } else {
            throw ClientException.orderNotFound();
        }
    }
}
