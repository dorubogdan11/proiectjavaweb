package demo.service;

import demo.exception.CarException;
import demo.models.Car;
import demo.models.CarManufacturer;
import demo.repository.CarManufacturerRepository;
import demo.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarService {
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private CarManufacturerRepository manufacturerRepository;

    public Car createCar(Car newCar) {
        Optional<List<Car>> carsFromManufacturer = carRepository.findByManufacturerId(newCar.getManufacturer().getId());
        if (!carsFromManufacturer.isEmpty()) {
            for (Car car : carsFromManufacturer.get()) {
                if (car.getModel().equals(newCar.getModel()) &&
                car.getYear() == newCar.getYear() &&
                car.getEngineCapacity() == newCar.getEngineCapacity()) {
                    throw CarException.carAlreadyExists();
                }
            }
        }
        return carRepository.save(newCar);
    }

    public Car createCar(long manufacturerId, Car newCar) {
        Optional<CarManufacturer> manufacturer = manufacturerRepository.findById(manufacturerId);
        if (manufacturer.isPresent()) {
            newCar.setManufacturer(manufacturer.get());
            return createCar(newCar);
        } else {
            throw  CarException.carManufacturerNotFound();
        }
    }

    public Car updateCar(Car newCar, Optional<Long> manufacturerId) {
        Optional<Car> car = carRepository.findById(newCar.getId());
        if (car.isPresent()) {
            if (manufacturerId.isPresent()) {
                Optional<CarManufacturer> newManufacturer = manufacturerRepository.findById(manufacturerId.get());
                if (newManufacturer.isPresent()) {
                    newCar.setManufacturer(newManufacturer.get());
                    car.get().update(newCar);
                    return carRepository.save(car.get());
                } else {
                    throw  CarException.carManufacturerNotFound();
                }
            } else {
                newCar.setManufacturer(car.get().getManufacturer());
                car.get().update(newCar);
                return carRepository.save(car.get());
            }
        } else {
            throw CarException.carNotFound();
        }
    }

    public List<Car> getAllCars() {
        List<Car> cars = (List<Car>) carRepository.findAll();
        return cars;
    }

    public List<Car> getCars(String manufacturer, String model, int year, int engineCapacity) {
        List<Car> cars = (List<Car>) carRepository.findAll();
        return cars.stream().filter(car -> isMatch(car, manufacturer, model, year, engineCapacity)).collect(Collectors.toList());
    }

    private boolean isMatch(Car car, String manufacturer, String model, int year, int engineCapacity) {
        return (manufacturer == null || car.getManufacturer().getManufacturerName().startsWith(manufacturer.toLowerCase()))
                && (model == null || car.getModel().startsWith(model.toLowerCase()))
                && (year == 0 || car.getYear() == year)
                && (engineCapacity == 0 || car.getEngineCapacity() == engineCapacity);
    }
}
