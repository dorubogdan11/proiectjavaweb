package demo.service;

import demo.exception.CarManufacturerException;
import demo.exception.CarPartException;
import demo.exception.PartManufacturerException;
import demo.models.CarPart;
import demo.models.PartManufacturer;
import demo.repository.PartManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PartManufacturerService {
    @Autowired
    private PartManufacturerRepository partManufacturerRepository;
    @Autowired
    private CarPartService carPartService;

    public PartManufacturer createPartManufacturer(PartManufacturer partManufacturer) {
        String partManufactureName = partManufacturer.getName().toLowerCase();
        partManufacturer.setName(partManufactureName);
        Optional<PartManufacturer> repoManufacturer = partManufacturerRepository.findByName(partManufactureName);
        if (!repoManufacturer.isPresent()) {
            PartManufacturer savedManufacturer = partManufacturerRepository.save(partManufacturer);
            saveCarParts(savedManufacturer, List.copyOf(partManufacturer.getParts()));

            return savedManufacturer;
        } else {
            throw PartManufacturerException.manufacturerAlreadyExists();
        }
    }

    public PartManufacturer updateManufacturer(Long id, List<CarPart> carParts) {
        Optional<PartManufacturer> manufacturer = partManufacturerRepository.findById(id);
        if (manufacturer.isPresent()) {
            saveCarParts(manufacturer.get(), carParts);
            return partManufacturerRepository.findById(id).get();
        } else {
            throw PartManufacturerException.manufactureNotFoundException();
        }
    }

    public void saveCarParts(PartManufacturer manufacturer, List<CarPart> carParts) {
        for (CarPart carPart : carParts) {
            carPart.setManufacturer(manufacturer);
            try {
                carPartService.createPart(carPart);
            } catch(CarPartException ignored) {}
        }
    }

    public PartManufacturer getPartManufacturer(String name) {
        Optional<PartManufacturer> manufacturer = partManufacturerRepository.findByName(name.toLowerCase());
        if (manufacturer.isPresent()) {
            return manufacturer.get();
        } else {
            throw PartManufacturerException.manufactureNotFoundException();
        }
    }

    public List<PartManufacturer> getAllManufacturers() {
        List<PartManufacturer> manufacturers = (List<PartManufacturer>) partManufacturerRepository.findAll();
        if (manufacturers.isEmpty()) {
            throw PartManufacturerException.manufacturesNotFoundException();
        } else {
            return manufacturers;
        }
    }
}
