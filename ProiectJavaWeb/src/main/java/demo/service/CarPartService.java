package demo.service;

import demo.exception.CarPartException;
import demo.models.Car;
import demo.models.CarPart;
import demo.models.PartManufacturer;
import demo.repository.CarPartRepository;
import demo.repository.CarRepository;
import demo.repository.PartManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarPartService {
    @Autowired
    private CarPartRepository carPartRepository;
    @Autowired
    private PartManufacturerRepository partManufacturerRepository;
    @Autowired
    private CarRepository carRepository;

    public CarPart createPart(CarPart newCarPart) {
        Optional<List<CarPart>> partsFromManufacturer = carPartRepository.findByPartManufacturerId(newCarPart.getManufacturer().getId());
        if (!partsFromManufacturer.isEmpty()) {
            for (CarPart carPart: partsFromManufacturer.get()) {
                if (carPart.getName().equals(newCarPart.getName())) {
                    throw CarPartException.partAlreadyExists();
                }
            }
        }
        return carPartRepository.save(newCarPart);
    }

    public CarPart createPart(long manufacturerId, CarPart newCarPart) {
        Optional<PartManufacturer> manufacturer = partManufacturerRepository.findById(manufacturerId);
        if (manufacturer.isPresent()) {
            newCarPart.setManufacturer(manufacturer.get());
            return createPart(newCarPart);
        } else {
            throw  CarPartException.partManufacturerNotFound();
        }
    }

    public CarPart updatePart(CarPart newCarPart, Optional<Long> manufacturerId) {
        Optional<CarPart> carPart = carPartRepository.findById(newCarPart.getId());
        if (carPart.isPresent()) {
            if (manufacturerId.isPresent()) {
                Optional<PartManufacturer> newManufacturer = partManufacturerRepository.findById(manufacturerId.get());
                if (newManufacturer.isPresent()) {
                    newCarPart.setManufacturer(newManufacturer.get());
                    carPart.get().update(newCarPart);
                    return carPartRepository.save(carPart.get());
                } else {
                    throw  CarPartException.partManufacturerNotFound();
                }
            } else {
                newCarPart.setManufacturer(carPart.get().getManufacturer());
                carPart.get().update(newCarPart);
                return carPartRepository.save(carPart.get());
            }
        } else {
            throw CarPartException.partNotFound();
        }
    }

    public List<CarPart> getAllCarParts() {
        List<CarPart> carParts = (List<CarPart>) carPartRepository.findAll();
        return carParts;
    }

    public CarPart addCompatibleCar(Long partId, Long carId) {
        Optional<CarPart> carPart = carPartRepository.findById(partId);
        if (carPart.isPresent()) {
            Optional<Car> car = carRepository.findById(carId);
            if (car.isPresent()) {
                carPart.get().addCompatibleCar(car.get());
                return carPartRepository.save(carPart.get());
            } else {
                throw CarPartException.carNotFound();
            }
        } else {
            throw CarPartException.partNotFound();
        }
    }

    public List<CarPart> getParts(String partManufacturer, String partName, String carManufacturer, String carModel) {
        List<CarPart> parts = (List<CarPart>) carPartRepository.findAll();
        return parts.stream().filter(part -> isMatch(part, partManufacturer, partName, carManufacturer, carModel)).collect(Collectors.toList());
    }

    public boolean isMatch(CarPart carPart, String partManufacturer, String partName, String carManufacturer, String carModel) {
        return (partManufacturer == null || carPart.getManufacturer().getName().startsWith(partManufacturer.toLowerCase()))
                && (partName == null || carPart.getName().startsWith(partName.toLowerCase()))
                && (carManufacturer == null || carPart.findCompatibleCarByManufacturerName(carManufacturer))
                && (carModel == null || carPart.findCompatibleCarByModel(carModel));
    }

}
