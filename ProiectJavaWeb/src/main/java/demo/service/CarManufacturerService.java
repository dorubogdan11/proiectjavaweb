package demo.service;

import demo.exception.CarException;
import demo.exception.CarManufacturerException;
import demo.models.Car;
import demo.models.CarManufacturer;
import demo.repository.CarManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarManufacturerService {
    @Autowired
    private CarManufacturerRepository carManufacturerRepository;

    @Autowired
    private CarService carService;

    public CarManufacturer createManufacturer(CarManufacturer carManufacturer) {
        String carManufactureName = carManufacturer.getManufacturerName().toLowerCase();
        carManufacturer.setManufacturerName(carManufactureName);
        Optional<CarManufacturer> repoManufacturer = carManufacturerRepository.findByManufacturerName(carManufactureName);
        if (!repoManufacturer.isPresent()) {
            CarManufacturer savedManufacturer = carManufacturerRepository.save(carManufacturer);
            saveCars(savedManufacturer, List.copyOf(carManufacturer.getCars()));

            return savedManufacturer;
        } else {
            throw CarManufacturerException.manufacturerAlreadyExists();
        }
    }

    public CarManufacturer updateManufacturer(Long id, List<Car> cars) {
        Optional<CarManufacturer> manufacturer = carManufacturerRepository.findById(id);
        if (manufacturer.isPresent()) {
            saveCars(manufacturer.get(), cars);
            return carManufacturerRepository.findById(id).get();
        } else {
            throw CarManufacturerException.manufactureNotFoundException();
        }
    }

    public void saveCars(CarManufacturer manufacturer, List<Car> cars) {
        for (Car car : cars) {
            car.setManufacturer(manufacturer);
            try {
                carService.createCar(car);
            } catch(CarException ignored) {}
        }
    }

    public CarManufacturer getCarManufacturer(String name) {
        Optional<CarManufacturer> manufacturer = carManufacturerRepository.findByManufacturerName(name.toLowerCase());
        if (manufacturer.isPresent()) {
            return manufacturer.get();
        } else {
            throw CarManufacturerException.manufactureNotFoundException();
        }
    }

    public List<CarManufacturer> getAllManufacturers() {
        List<CarManufacturer> manufacturers = (List<CarManufacturer>) carManufacturerRepository.findAll();
        if (manufacturers.isEmpty()) {
            throw CarManufacturerException.manufacturesNotFoundException();
        } else {
            return manufacturers;
        }
    }
}
