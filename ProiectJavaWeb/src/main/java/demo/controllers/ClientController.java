package demo.controllers;

import demo.exception.ClientException;
import demo.models.ClientOrder;
import demo.models.Clients;
import demo.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ClientController {
    @Autowired
    private ClientService clientService;

    @PostMapping(path="/createClient")
    public ResponseEntity addNewCar(@RequestBody Clients newClient) {
        try {
            Clients savedClient = clientService.createClient(newClient);
            return ResponseEntity.ok(savedClient);
        } catch (ClientException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PostMapping(path = "/addCarToClient/{carId}/{clientId}")
    public ResponseEntity addCarToClient(@PathVariable("carId") long carId, @PathVariable("clientId") long clientId) {
        try {
            Clients savedClient = clientService.addCar(clientId, carId);
            return ResponseEntity.ok(savedClient);
        } catch (ClientException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PostMapping(path = "/createNewOrder/{clientId}")
    public ResponseEntity createNewOrder(@PathVariable("clientId") long id) {
        try {
            ClientOrder newOrder = clientService.createNewOrder(id);
            return ResponseEntity.ok(newOrder);
        } catch (ClientException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PutMapping(path = "/addPartsToOrder/{partId}/{orderId}")
    public ResponseEntity addPartToOrder(@PathVariable("partId") long partId,
                                         @PathVariable("orderId") long orderId) {
        try {
            ClientOrder newOrder = clientService.addPartToOrder(orderId, partId);
            return ResponseEntity.ok(newOrder);
        } catch (ClientException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PutMapping(path = "/deletePartFromOrder/{partId}/{orderId}")
    public ResponseEntity deletePartFromOrder(@PathVariable("partId") long partId,
                                         @PathVariable("orderId") long orderId) {
        try {
            ClientOrder newOrder = clientService.deletePartFromOrder(orderId, partId);
            return ResponseEntity.ok(newOrder);
        } catch (ClientException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @DeleteMapping(path = "/deleteOrder/{orderId}")
    public ResponseEntity deleteOrder( @PathVariable("orderId") long orderId) {
        try {
            clientService.deleteOrder(orderId);
            return ResponseEntity.ok().build();
        } catch (ClientException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }
}
