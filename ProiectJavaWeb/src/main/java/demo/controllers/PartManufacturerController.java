package demo.controllers;

import demo.exception.PartManufacturerException;
import demo.models.CarPart;
import demo.models.PartManufacturer;
import demo.service.PartManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PartManufacturerController {
    @Autowired
    private PartManufacturerService partManufacturerService;

    @PostMapping(path="/partManufacturer")
    public ResponseEntity addPartManufacturer(@RequestBody PartManufacturer partManufacturer) {
        try {
            PartManufacturer savedPartManufacturer = partManufacturerService.createPartManufacturer(partManufacturer);
            return ResponseEntity.ok(savedPartManufacturer);
        } catch (PartManufacturerException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PutMapping(path = "/updatePartManufacturer/{id}")
    public ResponseEntity updatePartManufacturer(@PathVariable("id") long id, @RequestBody List<CarPart> carParts) {
        try {
            PartManufacturer updatedManufacturer = partManufacturerService.updateManufacturer(id, carParts);
            return ResponseEntity.ok(updatedManufacturer);
        } catch (PartManufacturerException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @GetMapping(value = "/getPartManufacturer/{name}")
    public ResponseEntity getPartManufacturer(@PathVariable("name") String name) {
        try {
            PartManufacturer manufacturer = partManufacturerService.getPartManufacturer(name);
            return ResponseEntity.ok(manufacturer);
        } catch (PartManufacturerException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @GetMapping(path = "/getAllPartManufacturers")
    public ResponseEntity getAllPartManufacturers() {
        try {
            List<PartManufacturer> manufacturers = partManufacturerService.getAllManufacturers();
            return ResponseEntity.ok(manufacturers);
        } catch (PartManufacturerException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }
}
