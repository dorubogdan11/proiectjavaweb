package demo.controllers;

import demo.exception.CarPartException;
import demo.models.CarPart;
import demo.service.CarPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CarPartController {
    @Autowired
    private CarPartService carPartService;

    @PostMapping(path="/addCarPart/{manufacturerId}")
    public ResponseEntity addNewCar(@PathVariable("manufacturerId") long id, @RequestBody CarPart newCarPart) {
        try {
            CarPart savedCarPart = carPartService.createPart(id, newCarPart);
            return ResponseEntity.ok(savedCarPart);
        } catch (CarPartException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PutMapping(path="/updateCarPart")
    public ResponseEntity updateCarPart(@RequestBody CarPart updatedCarPart) {
        try {
            CarPart savedCarPart = carPartService.updatePart(updatedCarPart, Optional.empty());
            return ResponseEntity.ok(savedCarPart);
        } catch (CarPartException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PutMapping(path = "/updateCarPart/{manufacturerId}")
    public ResponseEntity updateCarPartAndManufacturer(@PathVariable("manufacturerId") long id, @RequestBody CarPart updatedCarPart) {
        try {
            CarPart savedCarPart = carPartService.updatePart(updatedCarPart, Optional.of(id));
            return ResponseEntity.ok(savedCarPart);
        } catch (CarPartException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @GetMapping(path = "/getAllCarParts")
    public ResponseEntity getAllParts() {
        return  ResponseEntity.ok(carPartService.getAllCarParts());
    }

    @PostMapping(path = "/addCompatibleCar/{partId}/{carId}")
    public ResponseEntity addCompatibleCar(@PathVariable("partId") long partId, @PathVariable("carId") long carId) {
        try {
            CarPart savedCarPart = carPartService.addCompatibleCar(partId, carId);
            return ResponseEntity.ok(savedCarPart);
        } catch (CarPartException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @GetMapping(path = "/getParts")
    public ResponseEntity getParts(@RequestParam(required = false) String partManufacturer,
                                   @RequestParam(required = false) String partName,
                                   @RequestParam(required = false) String carManufacturer,
                                   @RequestParam(required = false) String carModel) {
        List<CarPart> parts = carPartService.getParts(partManufacturer, partName, carManufacturer, carModel);
        if(parts.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(parts);
        }
    }
}
