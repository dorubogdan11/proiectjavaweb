package demo.controllers;

import demo.exception.CarManufacturerException;
import demo.models.Car;
import demo.models.CarManufacturer;
import demo.service.CarManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CarManufacturerController {
    @Autowired
    private CarManufacturerService carManufacturerService;

    @PostMapping(path="/carManufacturer", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addCarManufacturer(@RequestBody CarManufacturer carManufacturer) {
        try {
            CarManufacturer savedCarManufacturer = carManufacturerService.createManufacturer(carManufacturer);
            return ResponseEntity.ok(savedCarManufacturer);
        } catch (CarManufacturerException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PutMapping(path = "/updateManufacturer/{id}")
    public ResponseEntity updateCarManufacturer(@PathVariable("id") long id, @RequestBody List<Car> cars) {
        try {
            CarManufacturer updatedManufacturer = carManufacturerService.updateManufacturer(id, cars);
            return ResponseEntity.ok(updatedManufacturer);
        } catch (CarManufacturerException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @GetMapping(value = "/getCarManufacturer/{name}")
    public ResponseEntity getCarManufacturer(@PathVariable("name") String name) {
        try {
            CarManufacturer manufacturer = carManufacturerService.getCarManufacturer(name);
            return ResponseEntity.ok(manufacturer);
        } catch (CarManufacturerException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @GetMapping(path = "/getAllManufacturers")
    public ResponseEntity getAllManufacturers() {
        try {
            List<CarManufacturer> manufacturers = carManufacturerService.getAllManufacturers();
            return ResponseEntity.ok(manufacturers);
        } catch (CarManufacturerException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }
}
