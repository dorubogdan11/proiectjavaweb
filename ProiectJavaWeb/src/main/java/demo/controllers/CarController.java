package demo.controllers;

import demo.exception.CarException;
import demo.models.Car;
import demo.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CarController {
    @Autowired
    private CarService carService;

    @PostMapping(path="/addCar/{manufacturerId}")
    public ResponseEntity addNewCar(@PathVariable("manufacturerId") long id, @RequestBody Car newCar) {
        try {
            Car savedCar = carService.createCar(id, newCar);
            return ResponseEntity.ok(savedCar);
        } catch (CarException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PutMapping(path="/updateCar")
    public ResponseEntity updateCar(@RequestBody Car updatedCar) {
        try {
            Car savedCar = carService.updateCar(updatedCar, Optional.empty());
            return ResponseEntity.ok(savedCar);
        } catch (CarException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @PutMapping(path = "/updateCar/{manufacturerId}")
    public ResponseEntity updateCarAndManufacturer(@PathVariable("manufacturerId") long id, @RequestBody Car updatedCar) {
        try {
            Car savedCar = carService.updateCar(updatedCar, Optional.of(id));
            return ResponseEntity.ok(savedCar);
        } catch (CarException e) {
            return ResponseEntity.badRequest().body(e.getError());
        }
    }

    @GetMapping(path = "/getAllCars")
    public ResponseEntity getAllCars() {
        return  ResponseEntity.ok(carService.getAllCars());
    }

    @GetMapping(path = "/getCars")
    public ResponseEntity getCars(@RequestParam(required = false) String manufacturer,
                                  @RequestParam(required = false) String model,
                                  @RequestParam(required = false) String year,
                                  @RequestParam(required = false) String engineCapacity) {
        int convertedYear, convertedEngineCapacity;
        try {
            convertedYear = Integer.parseInt(year);
        } catch (Exception e) {
            convertedYear = 0;
        }

        try {
            convertedEngineCapacity = Integer.parseInt(engineCapacity);
        } catch (Exception e) {
            convertedEngineCapacity = 0;
        }

        List<Car> cars = carService.getCars(manufacturer, model, convertedYear, convertedEngineCapacity);
        if(cars.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(cars);
        }
    }
}
