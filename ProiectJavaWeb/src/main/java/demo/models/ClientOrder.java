package demo.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="clientOrder")
public class ClientOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="clients_id", nullable = true)
    @JsonBackReference
    private Clients client;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "clientRrder_part",
            joinColumns = @JoinColumn(name="clientOrder_id"),
            inverseJoinColumns = @JoinColumn(name = "part_id"))
    private Set<CarPart> carParts;

    public ClientOrder() {
    }

    public void addPart(CarPart newPart) {
        carParts.add(newPart);
    }

    public void deleteAllParts() {
        this.carParts.clear();
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setClient(Clients clientsOrder) {
        this.client = clientsOrder;
    }

    public void setCarParts(Set<CarPart> carParts) {
        this.carParts = carParts;
    }

    public long getId() {
        return id;
    }

    public Clients getClient() {
        return client;
    }

    public Set<CarPart> getCarParts() {
        return carParts;
    }
}
