package demo.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="part_manufacturer")
public class PartManufacturer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name", nullable = false)
    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy="partManufacturer")
    private Set<CarPart> parts;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<CarPart> getParts() {
        return parts;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParts(Set<CarPart> parts) {
        this.parts = parts;
    }
}
