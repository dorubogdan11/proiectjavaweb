package demo.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="car_manufacturer")
public class CarManufacturer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name", nullable = false)
    private String manufacturerName;

    @JsonManagedReference
    @OneToMany(mappedBy="manufacturer")
    private Set<Car> cars;

    public long getId() { return id; }

    public CarManufacturer() {
    }

    public CarManufacturer(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public CarManufacturer(String manufacturerName, Set<Car> cars) {
        this.manufacturerName = manufacturerName;
        this.cars = cars;
    }

    public CarManufacturer(long id, String manufacturerName, Set<Car> cars) {
        this.id = id;
        this.manufacturerName = manufacturerName;
        this.cars = cars;
    }

    public String getManufacturerName() { return manufacturerName; }

    public Set<Car> getCars() { return cars; }

    public void setId(long id) { this.id = id; }

    public void setManufacturerName(String manufacturerName) { this.manufacturerName = manufacturerName; }

    public void setCars(Set<Car> cars) { this.cars = cars; }
}
