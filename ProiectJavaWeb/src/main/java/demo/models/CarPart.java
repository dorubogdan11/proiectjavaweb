package demo.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="part")
public class CarPart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="part_manufacturer_id", nullable = false)
    @JsonBackReference
    private PartManufacturer partManufacturer;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "car_part",
            joinColumns = @JoinColumn(name = "part_id"),
            inverseJoinColumns = @JoinColumn(name = "car_id"))
    private Set<Car> cars;

    @ManyToMany(mappedBy = "carParts")
    private Set<ClientOrder> orders;

    public void update(CarPart carPart) {
        if (carPart.name != null && !carPart.name.equals(this.name)) this.name = carPart.name;
        if (carPart.partManufacturer.getId() != this.partManufacturer.getId()) this.partManufacturer = carPart.partManufacturer;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public void addCompatibleCar(Car car) {
        cars.add(car);
    }

    public boolean findCompatibleCarByManufacturerName(String manufacturerName) {
        List<Car> list = cars.stream().filter(car -> car.getManufacturer().getManufacturerName().startsWith(manufacturerName)).collect(Collectors.toList());
        return !list.isEmpty();
    }

    public boolean findCompatibleCarByModel(String model) {
        List<Car> list = cars.stream().filter(car -> car.getModel().startsWith(model)).collect(Collectors.toList());
        return !list.isEmpty();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PartManufacturer getManufacturer() {
        return partManufacturer;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setManufacturer(PartManufacturer manufacturer) {
        this.partManufacturer = manufacturer;
    }
}
