package demo.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="model", nullable = false)
    private String model;

    @Column(name="year", nullable = false)
    private int year;

    @Column(name="engine_capacity", nullable = false)
    private int engineCapacity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="car_manufacturer_id", nullable = false)
    @JsonBackReference
    private CarManufacturer manufacturer;

    @ManyToMany(mappedBy = "cars")
    private Set<CarPart> parts;

    @ManyToMany(mappedBy = "clientCars")
    private Set<Clients> clients;

    public Car() {
    }

    public Car(String model, int year, int engineCapacity, CarManufacturer manufacturer) {
        this.model = model;
        this.year = year;
        this.engineCapacity = engineCapacity;
        this.manufacturer = manufacturer;
    }

    public Car(long id, String model, int year, int engineCapacity, CarManufacturer manufacturer) {
        this.id = id;
        this.model = model;
        this.year = year;
        this.engineCapacity = engineCapacity;
        this.manufacturer = manufacturer;
    }

    public void update(Car car) {
        if (car.model != null && !car.model.equals(this.model)) this.model = car.model;
        if (car.year != 0 && car.year != this.year) this.year = car.year;
        if (car.engineCapacity != 0 && car.engineCapacity != this.engineCapacity) this.engineCapacity = car.engineCapacity;
        if (car.manufacturer.getId() != this.manufacturer.getId()) this.manufacturer = car.manufacturer;
    }

    public long getId() { return id; }

    public CarManufacturer getManufacturer() { return manufacturer; }

    public String getModel() { return model; }

    public int getYear() { return year; }

    public int getEngineCapacity() { return engineCapacity; }

    public void setId(long id) { this.id = id; }

    public void setManufacturer(CarManufacturer manufacturer) { this.manufacturer = manufacturer; }

    public void setModel(String model) { this.model = model;}

    public void setYear(int year) { this.year = year; }

    public void setEngineCapacity(int engineCapacity) { this.engineCapacity = engineCapacity; }
}
