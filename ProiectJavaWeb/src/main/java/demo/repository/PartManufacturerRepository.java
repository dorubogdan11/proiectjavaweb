package demo.repository;

import demo.models.PartManufacturer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PartManufacturerRepository extends CrudRepository<PartManufacturer, Long> {
    Optional<PartManufacturer> findByName(String manufacturerName);
}
