package demo.repository;

import demo.models.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
    Optional<List<Car>> findByYear(int aint);
    Optional<List<Car>> findByEngineCapacity(int aint);
    Optional<List<Car>> findByManufacturerId(Long aLong);
}
