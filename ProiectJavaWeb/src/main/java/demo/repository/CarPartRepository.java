package demo.repository;

import demo.models.CarPart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarPartRepository extends CrudRepository<CarPart, Long> {
    Optional<List<CarPart>> findByPartManufacturerId(Long aLong);
}
