package demo.repository;

import demo.models.CarManufacturer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarManufacturerRepository extends CrudRepository<CarManufacturer, Long> {
    Optional<CarManufacturer> findByManufacturerName(String manufacturerName);
    Iterable<CarManufacturer> findAll();
}
